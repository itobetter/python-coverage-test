---
theme: seriph
background: './imgs/Testing-3-2.jpg'
class: text-center
shighlighter: shiki
lineNumbers: false
info: |
  ## Coverage Testing in python
  Presentation slides for developers.
drawings:
  persist: false
transition: slide-left
title: Testing in python
mdc: true
src: ./pages/1.md 

---
transition: fade-out 
---
src: ./pages/2.md
---
---
src: ./pages/3.md
---
---
src: ./pages/4.md
---
---
src: ./pages/5.md
---
---
src: ./pages/6.md
---
---
src: ./pages/7.md
---
---
src: ./pages/8.md
---
---
src: ./pages/9.md
---
---
src: ./pages/10.md
---

---
layout: center
class: text-center
---

# Demo Time